======
pakon-ws
======
A simple pipe between websocket clients and pakon-aggregator server (https://gitlab.labs.nic.cz/turris/pakon-aggregator).

It handles authentication from the clients. And it serves as bidirectional pipe between pakon-aggregator and clients.

Requirements
============
* websockets

Installation
============
python setup.py install

Running
=======
TODO
