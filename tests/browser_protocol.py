import websockets
import json


class ClientProtocol(websockets.WebSocketClientProtocol):
    def __init__(self, *args, **kwargs):
        super(ClientProtocol, self).__init__(*args, **kwargs)

    def print_sending(self, data):
        print('Sendig data: {}'.format(data))

    def print_recieved(self, data):
        print('Data recieved: {}'.format(data))

    async def send_subscribe(self):
        data = json.dumps({'method': 'subscribe'})
        self.print_sending(data)
        await self.send(data)

    async def send_unsubscribe(self):
        data = json.dumps({'method': 'unsubscribe'})
        self.print_sending(data)
        await self.send(data)

    async def recieve_data(self):
        while True:
            await self.recieve_data_single()

    async def recieve_data_single(self):
        data = await self.recv()
        self.print_recieved(data)

    def connection_lost(self, *args, **kwargs):
        print('Connection lost!')
        super(ClientProtocol, self).connection_lost(*args, **kwargs)
