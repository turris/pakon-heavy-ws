#!/usr/bin/env python3

import asyncio
import websockets

from browser_protocol import ClientProtocol

address = 'ws://localhost:8008'

# connect to ws server
async def connect():
    return await websockets.connect(address, klass=ClientProtocol)

loop = asyncio.get_event_loop()

try:
    connection = loop.run_until_complete(connect())
    loop.run_until_complete(connection.send_subscribe())
    loop.run_until_complete(connection.recieve_data_single())
    loop.run_until_complete(connection.recieve_data_single())
finally:
    if connection.open:
        loop.run_until_complete(connection.close())

try:
    connection = loop.run_until_complete(connect())
    loop.run_until_complete(connection.send_subscribe())
    loop.run_until_complete(connection.recieve_data_single())
    loop.run_until_complete(connection.recieve_data_single())
finally:
    if connection.open:
        loop.run_until_complete(connection.close())
