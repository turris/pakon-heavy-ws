#!/bin/sh

# Start multiple instances of the client
# and observer whether agregator sends data to both instances


cd "$(dirname "$0")"

export PYTHONPATH=$PYTHONPATH:..:../../

rm -rf /tmp/aggregator.soc
mkdir -p logs

python3 -u "../../bin/pakon-ws" > "logs/ws.log" 2>&1 &
( sleep 3 && python3 -u "browser.py" > "logs/browser1.log" 2>&1 ) &
( sleep 3 && python3 -u "browser.py" > "logs/browser2.log" 2>&1 ) &
sleep 2

expect << EXPECT
log_file -noappend logs/aggregator.log

spawn python3 -u aggregator.py

expect {
	"Subscribed: 1" {}
	"Subscribed: 2" {}
	timeout { exit 1 }
}

expect {
	"Subscribed: 1" {}
	"Subscribed: 2" {}
	timeout { exit 1 }
}

expect {
	"Sending data: (1)" {}
	"Sending data: (2)" {}
	timeout { exit 1 }
}

expect {
	"Sending data: (1)" {}
	"Sending data: (2)" {}
	timeout { exit 1 }
}

EXPECT

if [ "$?" = 0 ] ; then
	echo PASSED
else
	echo FAILED
fi

pkill -f '^python.*browser.py'
sleep 1
pkill -f '^python.*pakon-ws'
sleep 1
pkill -f '^python.*aggregator.py'
sleep 1
