import asyncio
import json

import random

subscribed = []


class ServerProtocol(asyncio.Protocol):
    id = 0

    def connection_made(self, transport):
        ServerProtocol.id += 1
        self.id = ServerProtocol.id
        print('Client {} connected'.format(self.id))
        self.transport = transport
        self.task = asyncio.Task(self.send_data())

    def data_received(self, data):
        print('Data received: {}'.format(data))
        try:
            message = data.decode()
            element = json.loads(message)
            if element['method'] == 'subscribe' and self not in subscribed:
                subscribed.append(self)
                print('Subscribed: {}'.format(self.id))
            elif element['method'] == 'unsubscribe' and self in subscribed:
                subscribed.remove(self)
                print('Unsubscribed: {}'.format(self.id))
        except:
            pass

    def connection_lost(self, exc):
        self.task.cancel()
        if self in subscribed:
            subscribed.remove(self)
            print('Unsubscribed: {}'.format(self.id))
        print('Client {} disconnected'.format(self.id))

    def get_data(self):
        return json.dumps({'data': random.randint(1, 100000)})

    async def send_data(self):
        while True:
            await asyncio.sleep(random.randint(1, 5))
            data = self.get_data()
            data += '\n'
            if self in subscribed:
                print("Sending data: ({}) {}".format(self.id, data))
                self.transport.write(data.encode())
