#!/bin/sh

# ws should disconnect clients when a establishing connection to aggregator fails


cd "$(dirname "$0")"

export PYTHONPATH=$PYTHONPATH:..:../../

rm -rf /tmp/aggregator.soc
mkdir -p logs

python3 -u "../../bin/pakon-ws" > "logs/ws.log" 2>&1 &
sleep 2

expect << EXPECT
log_file -noappend logs/browser.log

spawn python3 -u browser.py

expect {
	"Sendig data:" {}
	timeout { exit 1 }
}

expect {
	"Connection lost!" { exit 0 }
	timeout { exit 1 }
}
EXPECT

if [ "$?" = 0 ] ; then
	echo PASSED
else
	echo FAILED
fi

pkill -f '^python.*browser.py'
sleep 1
pkill -f '^python.*pakon-ws'
sleep 1
pkill -f '^python.*aggregator.py'
sleep 1
