#!/usr/bin/env python3

import asyncio
import websockets
import time

from browser_protocol import ClientProtocol

address = 'ws://localhost:8008'

# connect to ws server
async def connect():
    return await websockets.connect(address, klass=ClientProtocol)

loop = asyncio.get_event_loop()
connection = loop.run_until_complete(connect())

try:
    loop.run_until_complete(connection.send_unsubscribe())
    time.sleep(1)
    loop.run_until_complete(connection.send_subscribe())
    loop.run_until_complete(connection.recieve_data_single())
    loop.run_until_complete(connection.recieve_data_single())
    loop.run_until_complete(connection.recieve_data_single())
    loop.run_until_complete(connection.send_unsubscribe())
    time.sleep(1)
    loop.run_until_complete(connection.send_subscribe())
    loop.run_until_complete(connection.recieve_data())
finally:
    if connection.open:
        loop.run_until_complete(connection.close())
