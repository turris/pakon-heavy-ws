#!/bin/sh

# This is a simple use-case
#
# start pakon-ws and agregator mock
#
# and try to subcribe and unsubscribe as a client to a data stream
#


cd "$(dirname "$0")"

export PYTHONPATH=$PYTHONPATH:..:../../

rm -rf /tmp/aggregator.soc
mkdir -p logs

python3 -u "aggregator.py" > "logs/aggregator.log" 2>&1 &
python3 -u "../../bin/pakon-ws" > "logs/ws.log" 2>&1 &
sleep 2

expect << EXPECT
log_file -noappend logs/browser.log

spawn python3 -u browser.py

expect {
	"Sendig data:" {}
	timeout { exit 1 }
}

expect {
	"Data recieved:" {}
	timeout { exit 1 }
}

expect {
	"Data recieved:" {}
	timeout { exit 1 }
}

expect {
	"Data recieved:" {}
	timeout { exit 1 }
}

expect {
	"Sendig data:" {}
	timeout { exit 1 }
}

expect {
	"Sendig data:" {}
	timeout { exit 1 }
}

expect {
	"Data recieved:" {}
	timeout { exit 1 }
}
EXPECT

if [ "$?" = 0 ] ; then
	echo PASSED
else
	echo FAILED
fi

pkill -f '^python.*browser.py'
sleep 1
pkill -f '^python.*pakon-ws'
sleep 1
pkill -f '^python.*aggregator.py'
sleep 1
