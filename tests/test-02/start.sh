#!/bin/sh

# When a browser disconnects the ws instance should end properly
# and a proper message should be displayed in aggregator
#

cd "$(dirname "$0")"

export PYTHONPATH=$PYTHONPATH:..:../../

rm -rf /tmp/aggregator.soc
mkdir -p logs

python3 -u "../../bin/pakon-ws" > "logs/ws.log" 2>&1 &
( sleep 3 && python3 -u "browser.py" > "logs/browser.log" 2>&1 ) &
sleep 2

expect << EXPECT
log_file -noappend logs/aggregator.log

spawn python3 -u aggregator.py

expect {
	"Subscribed: 1" {}
	timeout { exit 1 }
}

expect {
	"Sending data:" {}
	"Unsubscribed: 1" {}
	timeout { exit 1 }
}

expect {
	"Client 1 disconnected" { exit 0 }
	"Sending data:" { exit 1 }
	timeout { exit 1 }
}
EXPECT

if [ "$?" = 0 ] ; then
	echo PASSED
else
	echo FAILED
fi

pkill -f '^python.*browser.py'
sleep 1
pkill -f '^python.*pakon-ws'
sleep 1
pkill -f '^python.*aggregator.py'
sleep 1
