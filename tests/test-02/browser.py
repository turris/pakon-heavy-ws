#!/usr/bin/env python3

import asyncio
import websockets

import sys

from browser_protocol import ClientProtocol


class ClientProtocolCustom(ClientProtocol):
    async def recieve_data(self):
        while True:
            data = await self.recv()
            self.print_recieved(data)
            # shutdown immediately
            print("Shutting down.")
            sys.exit(1)

address = sys.argv[1] if len(sys.argv) == 2 else 'ws://localhost:8008'

# connect to ws server
async def connect():
    return await websockets.connect(address, klass=ClientProtocolCustom)

loop = asyncio.get_event_loop()
connection = loop.run_until_complete(connect())

try:
    loop.run_until_complete(asyncio.gather(connection.send_subscribe(), connection.recieve_data()))
finally:
    if connection.open:
        loop.run_until_complete(connection.close())
