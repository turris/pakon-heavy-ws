#!/usr/bin/env python3


import asyncio
import os
import random
import sys

from aggregator_protocol import ServerProtocol, subscribed

sock_path = '/tmp/aggregator.soc'


class ServerProtocolCustom(ServerProtocol):
    async def send_data(self):
        while True:
            await asyncio.sleep(random.randint(1, 5))
            data = self.get_data()
            if self in subscribed:
                print("Sending data: {}".format(data))
                self.transport.write(data.encode())
                # Exit immediatelly after sending data
                sys.exit()


loop = asyncio.get_event_loop()

# Each client connection will create a new protocol instance
coro = loop.create_unix_server(ServerProtocolCustom, sock_path)
server = loop.run_until_complete(coro)

# Serve requests until CTRL+c is pressed
print('Serving on {}'.format(server.sockets[0].getsockname()))
try:
    loop.run_forever()
finally:
    server.close()
    loop.close()
    os.unlink(sock_path)
