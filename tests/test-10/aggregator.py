#!/usr/bin/env python3


import asyncio
import os
import random

from aggregator_protocol import ServerProtocol, subscribed

sock_path = '/tmp/aggregator.soc'


class ServerProtocolCustom(ServerProtocol):
    async def send_data(self):
        count = 0
        while True:
            await asyncio.sleep(random.randint(1, 5))
            data = [u"žluťoučký", b"\xf0\x28\x8c\xbc", "normal"]
            if self in subscribed:
                data = data[count % len(data)]
                data += '\n' if isinstance(data, str) else b'\n'
                print("Sending data: {}".format(data))
                self.transport.write(data if type(data) == bytes else data.encode())
                count += 1


loop = asyncio.get_event_loop()

# Each client connection will create a new protocol instance
coro = loop.create_unix_server(ServerProtocolCustom, sock_path)
server = loop.run_until_complete(coro)

# Serve requests until CTRL+c is pressed
print('Serving on {}'.format(server.sockets[0].getsockname()))
try:
    loop.run_forever()
except Exception as e:
    print(e)
finally:
    server.close()
    loop.close()
    os.unlink(sock_path)
