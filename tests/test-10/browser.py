#!/usr/bin/env python3

import asyncio
import websockets

import sys

from browser_protocol import ClientProtocol


class ClientProtocolCustom(ClientProtocol):
    async def send_raw_data(self):
        data = b'\xf0\x28\x8c\x28'
        self.print_sending(data)
        await self.send(data)

    async def send_utf8(self):
        data = u'kůň pěl'
        self.print_sending(data)
        await self.send(data)


address = sys.argv[1] if len(sys.argv) == 2 else 'ws://localhost:8008'

# connect to ws server
async def connect():
    return await websockets.connect(address, klass=ClientProtocolCustom)

loop = asyncio.get_event_loop()
connection = loop.run_until_complete(connect())

try:
    loop.run_until_complete(connection.send_subscribe())
    loop.run_until_complete(connection.send_utf8())
    loop.run_until_complete(connection.send_raw_data())
    loop.run_until_complete(connection.recieve_data_single())
    loop.run_until_complete(connection.recieve_data_single())
    loop.run_until_complete(connection.recieve_data_single())
finally:
    if connection.open:
        loop.run_until_complete(connection.close())
