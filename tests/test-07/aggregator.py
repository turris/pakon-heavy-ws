#!/usr/bin/env python3


import os
import asyncio

from aggregator_protocol import ServerProtocol

subscribed = []


sock_path = '/tmp/aggregator.soc'


loop = asyncio.get_event_loop()

# Each client connection will create a new protocol instance
coro = loop.create_unix_server(ServerProtocol, sock_path)
server = loop.run_until_complete(coro)

# Serve requests until CTRL+c is pressed
print('Serving on {}'.format(server.sockets[0].getsockname()))
try:
    loop.run_forever()
finally:
    server.close()
    loop.close()
    os.unlink(sock_path)
