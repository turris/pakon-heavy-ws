#!/bin/sh

# Tokens test
#
# Set the token dir.
# Put one valid and one invalid token inside
# Test for valid, non-existing and invalid
# Check whether the invalid token was removed
#


cd "$(dirname "$0")"

export PYTHONPATH=$PYTHONPATH:..:../../

rm -rf /tmp/aggregator.soc
mkdir -p logs
mkdir -p /tmp/pakon-tokens
touch /tmp/pakon-tokens/pass
touch -t 201501010000 /tmp/pakon-tokens/invalid

python3 -u "aggregator.py" > "logs/aggregator.log" 2>&1 &
python3 -u "../../bin/pakon-ws" -t /tmp/pakon-tokens/ --token-prefix custom-prefix > "logs/ws.log" 2>&1 &
sleep 2

expect << EXPECT
log_file -noappend logs/browser.log

spawn python3 -u browser.py

expect {
	"Sendig data:" { exit 1}
	"Data recieved:" { exit 1}
	"Connection lost!" {}
	timeout { exit 1 }
}

expect {
	"Sendig data:" { exit 1}
	"Data recieved:" { exit 1}
	"Connection lost!" {}
	timeout { exit 1 }
}

expect {
	"Sendig data:" { exit 1}
	"Data recieved:" { exit 1}
	"Connection lost!" {}
	timeout { exit 1 }
}

expect {
	"Sendig data:" {}
	timeout { exit 1 }
}

expect {
	"Data recieved:" {}
	timeout { exit 1 }
}

expect {
	"Data recieved:" {}
	timeout { exit 1 }
}

expect {
	"Connection lost!" {}
	timeout { exit 1 }
}
EXPECT

if [ "$?" = 0 ] ; then
	if [ -f /tmp/pakon-tokens/invalid ] ; then
		echo "invalid token wasn't deleted"
		echo FAILED
	else
		echo PASSED
	fi
else
	echo FAILED
fi

rm -rf /tmp/pakon-tokens

pkill -f '^python.*browser.py'
sleep 1
pkill -f '^python.*pakon-ws'
sleep 1
pkill -f '^python.*aggregator.py'
sleep 1
