#!/usr/bin/env python3

import asyncio
import websockets

from browser_protocol import ClientProtocol

address_pass = 'ws://localhost:8008/custom-prefix/pass'
address_invalid = 'ws://localhost:8008/custom-prefix/invalid'
address_fail = 'ws://localhost:8008/custom-prefix/fail'
address_prefix_fail = 'ws://localhost:8008/fail-prefix/pass'

async def connect(address):
    return await websockets.connect(address, klass=ClientProtocol)

loop = asyncio.get_event_loop()

connection = None
try:
    connection = loop.run_until_complete(connect(address_fail))
    loop.run_until_complete(connection.send_subscribe())
    loop.run_until_complete(connection.recieve_data_single())
    loop.run_until_complete(connection.recieve_data_single())
except websockets.exceptions.InvalidHandshake:
    pass
finally:
    if connection and connection.open:
        loop.run_until_complete(connection.close())

try:
    connection = loop.run_until_complete(connect(address_invalid))
    loop.run_until_complete(connection.send_subscribe())
    loop.run_until_complete(connection.recieve_data_single())
    loop.run_until_complete(connection.recieve_data_single())
except websockets.exceptions.InvalidHandshake:
    pass
finally:
    if connection and connection.open:
        loop.run_until_complete(connection.close())

try:
    connection = loop.run_until_complete(connect(address_prefix_fail))
    loop.run_until_complete(connection.send_subscribe())
    loop.run_until_complete(connection.recieve_data_single())
    loop.run_until_complete(connection.recieve_data_single())
except websockets.exceptions.InvalidHandshake:
    pass
finally:
    if connection and connection.open:
        loop.run_until_complete(connection.close())

try:
    connection = loop.run_until_complete(connect(address_pass))
    loop.run_until_complete(connection.send_subscribe())
    loop.run_until_complete(connection.recieve_data_single())
    loop.run_until_complete(connection.recieve_data_single())
finally:
    if connection and connection.open:
        loop.run_until_complete(connection.close())
