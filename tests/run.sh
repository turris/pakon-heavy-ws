#!/bin/sh

cd "$(dirname "$0")"

for dirname in test-* ; do
	echo "====== $dirname ======"
	./"$dirname"/start.sh
	echo "****** $dirname ******"
done
