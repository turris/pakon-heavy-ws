import os
import time
import websockets


class WebSocketServerProtocolBase(websockets.WebSocketServerProtocol):
    id = 0
    aggregator_protocol = None

    async def forward_to_aggregator(self, data):
        print("({}) Forwarding data to aggregator: {}".format(self.id, data))
        if self.aggregator_protocol:
            data = data if type(data) == bytes else bytearray(data, 'utf8')
            self.aggregator_protocol.writer.write(data)
            await self.aggregator_protocol.writer.drain()

    def connection_made(self, transport):
        WebSocketServerProtocolBase.id += 1
        self.id = WebSocketServerProtocolBase.id
        print("({}) Client connected".format(self.id))
        super(WebSocketServerProtocolBase, self).connection_made(transport)
        self.transport = transport

    def connection_lost(self, exc):
        print("({}) Client connection closed.".format(self.id))
        super(WebSocketServerProtocolBase, self).connection_lost(exc)

        # close the connection to aggregator and quit
        if self.aggregator_protocol:
            self.aggregator_protocol.close()

    def check_token(self, token):
        result = False
        current_time = time.time()
        try:
            if self.token_dir:
                to_delete = []
                for entry in os.scandir(self.token_dir):
                    if current_time - entry.stat().st_mtime > self.token_valid_period:
                        to_delete.append(entry.name)
                        continue
                    if token == entry.name:
                        result = True

                # delete invalid tokens
                for file_name in to_delete:
                    os.unlink(os.path.join(self.token_dir, file_name))
            else:
                # no directory set
                return True

        except Exception:
            print("Token management failed.")

        return result

    def is_allowed_to_connect(self, path, headers):

        # remove the prefix if it exists
        prefix = self.path_prefix.strip('/')
        path = path.strip('/')
        token = path[len(prefix):] if path.startswith(prefix) else ''
        token = token.strip('/')

        # When no token_dir is set -> skip token validation
        if self.token_dir and (not token or not self.check_token(token)):
            print("({}) Token authentication failed. (sending 401)".format(self.id))
            raise websockets.exceptions.NotAllowedToConnect("Wrong token")


def websocket_server_protocol_gen(token_dir=None, token_valid_period=None, path_prefix='/'):

    return type(
        'WebSocketServerProtocol',
        (WebSocketServerProtocolBase, ),
        {
            'token_dir': token_dir,
            'token_valid_period': token_valid_period,
            'path_prefix': path_prefix,
        },
    )
