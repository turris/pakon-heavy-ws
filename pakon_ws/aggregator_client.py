import asyncio


class AggregatorClientProtocol(asyncio.StreamReaderProtocol):
    websocket_protocol = None
    writer = None
    reader = None

    def __init__(self, websocket_protocol, loop, *args, **kwargs):
        self.loop = loop
        self.reader = asyncio.StreamReader(loop=loop)
        self.websocket_protocol = websocket_protocol
        super(AggregatorClientProtocol, self).__init__(self.reader, *args, **kwargs)

    def connection_made(self, transport):
        print("({}) Connected to aggregator.".format(self.websocket_protocol.id))
        super(AggregatorClientProtocol, self).connection_made(transport)
        self.writer = asyncio.StreamWriter(transport, self, self.reader, self.loop)

    async def forward_to_ws_client(self):
        try:
            while True:
                # This will read data until '\n' is found
                data = await self.reader.readuntil()
                print("({}) Forwarding data to client: {}".format(
                    self.websocket_protocol.id, data))
                await self.websocket_protocol.send(data)

        except asyncio.streams.IncompleteReadError:
            self.close()
            # Close the connection to the ws client
            await self.websocket_protocol.close()

    def data_received(self, data):
        print("({}) Recieved data from aggregator: {}".format(self.websocket_protocol.id, data))
        super(AggregatorClientProtocol, self).data_received(data)

    def close(self):
        self.writer and self.writer.close()

    def connection_lost(self, exc):
        print("({}) Aggregator connection closed.".format(self.websocket_protocol.id))
        super(AggregatorClientProtocol, self).connection_lost(exc)

        # close the connection to ws client (it will perform the closing handshake)
        asyncio.ensure_future(self.websocket_protocol.close())
